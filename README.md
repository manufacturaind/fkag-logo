# Free Knowledge Advocacy Group logo


![Free Knowledge Advocacy Group logo](https://gitlab.com/manufacturaind/fkag-logo/-/raw/master/bitmap%20-%20png/web/fkag_color-transparent-background.png)

Logo of the [Free Knowledge Advocacy Group](https://meta.wikimedia.org/wiki/EU_policy)  
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0](http://creativecommons.org/licenses/by-sa/4.0/) International License</a>.


### Colours

Reference for the colors:
* raspberry: CMYK(0, 55, 55, 17); RGB #d35f5f
* black
* almost gray: CMYK (0, 4, 4, 11); RBG #e3dbdb


### Typography

[Anton font](https://fonts.google.com/specimen/Anton?query=anton), by Vernon Adams, published under the Open Font License.

  > Anton is a reworking of many traditional advertising sans serif typefaces. The letter forms have been digitised and then reshaped for use as a webfont, the counters have been opened up a little and the stems optimised for use as bold display font in modern web browsers. 
  

### Other sources

The **texture** used in the logo was created by editing a photo - [Sand](https://www.flickr.com/photos/akras/32579317904/) - by Andrey, published under a [CC BY 2.0 license](https://creativecommons.org/licenses/by/2.0/).


### Repository structure

* `vector` - folder containing the SVG versions of the logo. See file `fkag_all-variations.svg` for a quick glance at the options (here the logo is over a light blue background to show transparencies).
* `bitmap - png` - this folder has PNG exports of the logo, and is organised in two folders `web` -- 300px wide logo exports to use in the web -- and `print` -- larger versions with 300dpi for print.

Here is a preview of several variations of the logo, in b&w, with and without transparency; with colour, with and without transparency; with background rectangle and without (white border was added for reference):

![Logo variations](https://gitlab.com/manufacturaind/fkag-logo/-/raw/master/bitmap%20-%20png/fkag_variations_small.png)
